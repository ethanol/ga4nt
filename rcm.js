function RCM(img) {
	this.elem = document.createElement('ul');

	function calcCoords(cursorCoords) {
		// Temporarly turning menu on, to measure it
		this.elem.parentElement.classList.add('hidden');
		this.elem.parentElement.style.display = 'block';
		let rcmDimensions = [this.elem.parentElement.clientHeight, this.elem.parentElement.clientWidth];
		this.elem.parentElement.style.display = '';
		this.elem.parentElement.classList.remove('hidden');

		let coordinates = {};
		// Calculating coordinates
		for (dimension in cursorCoords) {
			let clientDimension;
			let rcmDimension;
			if ( dimension == 'top' ) {
				clientDimension = document.documentElement.clientHeight;
				rcmDimension = rcmDimensions[0];
			} else {
				// if dimension == 'left'
				clientDimension = document.documentElement.clientWidth;
				rcmDimension = rcmDimensions[1];
			}
			// If space between cursor and page border <= dimension of menu + little bit
			if (clientDimension - cursorCoords[dimension] <= rcmDimension + 15) {
				// open menu to the other side, 5px are fine-tune
				coordinates[dimension] = `${cursorCoords[dimension] - rcmDimension - 5}px`;
			} else {
				// 5px for consistency
				coordinates[dimension] = `${cursorCoords[dimension] + 5}px`;
			}
		}
		return coordinates;
	}


	this.turn = function (state, cursorCoords) {
		switch (state) {
			case true:
				this.elem.parentElement.style.position = 'absolute';
				let { top, left } = calcCoords.call(this, cursorCoords);
				this.elem.parentElement.style.top = top;
				this.elem.parentElement.style.left = left;
				this.elem.parentElement.style.display = 'block';
				break;
		
			default:
				this.elem.parentElement.style.display = '';
				break;
		}
	}

	let buttons = [];

	// Title
	let frag = new DocumentFragment();
	let li = document.createElement('li');
	let a = document.createElement('a');
	a.textContent = browser.i18n.getMessage(img.title.replace(/-/g, ''));
	li.append(a)
	frag.append(li);
	buttons.push(frag);

	// Open on ghibli.jp
	frag = new DocumentFragment();
	li = document.createElement('li');
	a = document.createElement('a');
	a.textContent = browser.i18n.getMessage('openOnURL', 'ghibli.jp');
	a.href = `https://www.ghibli.jp/works/${img.title}/#frame&gid=1&pid=${img.number}`;
	li.append(a);
	frag.append(li);
	buttons.push(frag);

	// Save
	frag = new DocumentFragment();
	li = document.createElement('li');
	a = document.createElement('a');
	a.textContent = browser.i18n.getMessage('save');
	a.href = `${img.url}`;
	a.download = `${img.filename}`;
	li.append(a);
	frag.append(li);
	buttons.push(frag);

	// Pin/Unpin
	frag = new DocumentFragment();
	li = document.createElement('li');
	a = document.createElement('a');
	a.textContent = img.pinned ? browser.i18n.getMessage('unpin') : browser.i18n.getMessage('pin');
	a.onclick = (event) => {
		if (img.pinned) {
			img.pinned = false;
			localStorage.removeItem('pinned');
			event.target.textContent = browser.i18n.getMessage('pin');
		} else {
			img.pinned = true;
			localStorage.setItem('pinned', JSON.stringify(img))
			event.target.textContent = browser.i18n.getMessage('unpin');
		}
	}
	li.append(a);
	frag.append(li);
	buttons.push(frag);

	// Previous, if it exists
	if ('previous' in img) {
		frag = new DocumentFragment();
		li = document.createElement('li');
		a = document.createElement('a');
		a.textContent = browser.i18n.getMessage('previous');
		a.href = `https://www.ghibli.jp/works/${img.previous.title}/#frame&gid=1&pid=${img.previous.number}`;
		li.append(a);
		frag.append(li);
		buttons.push(frag);
	}

	// Settings
	frag = new DocumentFragment();
	li = document.createElement('li');
	a = document.createElement('a');
	a.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-compact-down" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M1.553 6.776a.5.5 0 0 1 .67-.223L8 9.44l5.776-2.888a.5.5 0 1 1 .448.894l-6 3a.5.5 0 0 1-.448 0l-6-3a.5.5 0 0 1-.223-.67z"/></svg>';
	a.onclick = (event) => {
		window.scrollTo({
			top: 99999,
			left: 0,
			behavior: 'smooth'
		});
		setTimeout( () => {
			document.removeEventListener('contextmenu', rcmHandler);
			document.removeEventListener('click', rcmHandler);			
		}, 100);
	}
	li.append(a);
	frag.append(li);
	buttons.push(frag);

	this.elem.append(...buttons);
}