const COVER_ELEM = document.querySelector('#cover');
const BG_ELEM = document.querySelector('#bg');
const RCM_ELEM = document.querySelector('nav');

let rcm;
function rcmHandler(event) {
	if (event.target == COVER_ELEM && event.type == 'contextmenu') {
		event.preventDefault();
		if (RCM_ELEM.style.display == '') {
			rcm.turn(true, {
				top: event.pageY,
				left: event.pageX,
			});
		} else rcm.turn(false);
	} else if ( (event.target == COVER_ELEM || (event.target.tagName == 'A' && document.querySelector('nav ul li:first-child a') != event.target)) && event.type == 'click') {
		rcm.turn(false);
	}
}

// Needed for anime list
let titlesState = {};
let titlesStateChanges = {};

function stateOfSaveBtn() {
	for (title in titlesState) {
		if (title in titlesStateChanges && titlesState[title].use != titlesStateChanges[title].use) {
			document.querySelector('div#settings div#buttons > button#save').classList.add('active');
			return;
		}
	}
	document.querySelector('div#settings div#buttons > button#save').classList.remove('active');
}

//
// Executions
//
let port;
Promise.all([
	new Promise( (resolve, reject) => {
		document.addEventListener('DOMContentLoaded', () => resolve() )
	}),
	new Promise( (resolve, reject) => {
		port = browser.runtime.connect();
		port.onMessage.addListener( message => {
			if (message == 'reload') {
				location.reload();
				resolve();
			} else {
				// Then it's an image
				resolve(message);
			}
		});
	}),
]).then( (resolved) => {
	let img = resolved[1];

	// If reloaded on settings page
	window.scrollTo({top: 0, left: 0})
	
	// Setting background
	BG_ELEM.style.backgroundImage = `url(${img.url})`;
	COVER_ELEM.style.background = 'transparent';

	// Setting up Right-Click Menu
	rcm = new RCM(img);
	RCM_ELEM.append(rcm.elem);
	document.addEventListener('contextmenu', rcmHandler);
	document.addEventListener('click', rcmHandler);
	
	// Setting up settings page "go back" button
	document.querySelector('div#settings header > p').addEventListener('click', () => {
		document.addEventListener('contextmenu', rcmHandler);
		document.addEventListener('click', rcmHandler);
		window.scrollTo({
			top: 0,
			left: 0,
			behavior: 'smooth'
		});
	});

	// Setting up settings page messages
	document.querySelector('div#settings header > p').innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-bar-up" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M3.646 11.854a.5.5 0 0 0 .708 0L8 8.207l3.646 3.647a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 0 0 0 .708zM2.4 5.2c0 .22.18.4.4.4h10.4a.4.4 0 0 0 0-.8H2.8a.4.4 0 0 0-.4.4z"/></svg>' + browser.i18n.getMessage('goback');
	document.querySelector('div#settings header h1').textContent = browser.i18n.getMessage('settings')
	document.querySelector('div#settings header h2').textContent = browser.i18n.getMessage('customList')

	document.querySelector('div#settings div#buttons > button#check').textContent = browser.i18n.getMessage('check');
	document.querySelector('div#settings div#buttons > button#check').addEventListener('click', () => {
		let isFalse;
		for (title in titlesState) {
			if (title in titlesStateChanges) {
				if (titlesStateChanges[title].use == false) {
					isFalse = true;
				}
			} else {
				if (titlesState[title].use == false) {
					isFalse = true;
				}
			}
		}
		if (isFalse) {
			for (title in titlesState) {
				titlesStateChanges[title] = {use: true};
				document.querySelectorAll('div#settings div#titles > div.title').forEach(element => {
					element.classList.add('active');
				});
			}
		} else {
			for (title in titlesState) {
				titlesStateChanges[title] = {use: false};
				document.querySelectorAll('div#settings div#titles > div.title').forEach(element => {
					element.classList.remove('active');
				});
			}
		}
		stateOfSaveBtn();
	});

	document.querySelector('div#settings div#buttons > button#discard').textContent = browser.i18n.getMessage('discard');
	document.querySelector('div#settings div#buttons > button#discard').addEventListener('click', () => {
		for (title in titlesState) {
			delete titlesStateChanges[title];
			if (titlesState[title].use == true) {
				document.querySelector(`div#settings div#titles > div.title#${title}`).classList.add('active')
			} else {
				document.querySelector(`div#settings div#titles > div.title#${title}`).classList.remove('active')
			}
		}
		stateOfSaveBtn();
	})

	document.querySelector('div#settings div#buttons > button#save').textContent = browser.i18n.getMessage('save');
	document.querySelector('div#settings div#buttons > button#save').addEventListener('click', () => {
		for (title in titlesState) {
			if (title in titlesStateChanges) {
				localStorage.setItem(title, JSON.stringify(titlesStateChanges[title]));
				titlesState[title] = titlesStateChanges[title];
				delete titlesStateChanges[title];
			} else {
				localStorage.setItem(title, JSON.stringify(titlesState[title]));
			}
		}
		stateOfSaveBtn();
		window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
		setTimeout( () => {
			port.postMessage('refetch');
		}, 500);
	});

	// Setting up settings page titles checkbox list
	img.allTitles.forEach( title => {
		// Making element
		let pElem = document.createElement('p');
		pElem.textContent = browser.i18n.getMessage(title.replace(/-/g, ''));
		let pElemDiv = document.createElement('div');
		pElemDiv.append(pElem);
		let imgElem = document.createElement('img');
		imgElem.src = `https://www.ghibli.jp/images/${title}.jpg`;
		let decorElem = document.createElement('div');
		decorElem.classList.add('decor');
		decorElem.append(document.createElement('div'));
		let titleElem = document.createElement('div');
		titleElem.classList.add('title');
		titleElem.id = `${title}`;
		
		let titleSaved = JSON.parse(localStorage.getItem(title));
		if (!titleSaved) {
			titlesState[title] = {
				use: true,
			};
			localStorage.setItem(title, JSON.stringify(titlesState[title]));
			titleElem.classList.add('active');
		} else if (titleSaved != null && 'use' in titleSaved && titleSaved.use == true) {
			titleElem.classList.add('active');
			titlesState[title] = titleSaved;
		} else if (titleSaved != null && 'use' in titleSaved && titleSaved.use == false) {
			titlesState[title] = titleSaved;
		}

		titleElem.append(decorElem, imgElem, pElemDiv);
		document.querySelector('div#settings div#titles').append(titleElem);
	});

	document.querySelectorAll('div#settings div#titles > div.title').forEach(element => {
		element.addEventListener('click', (event) => {
			let titleElem = event.target;
			if (!titleElem.classList.contains('title')) {
				do {
					titleElem = titleElem.parentElement;
				} while (!titleElem.classList.contains('title'));
			}
			titleElem.classList.toggle('active');
			if (titleElem.id in titlesStateChanges) {
				titlesStateChanges[titleElem.id] = {use: !titlesStateChanges[titleElem.id].use};
			} else {
				titlesStateChanges[titleElem.id] = {use: !titlesState[titleElem.id].use};
			}
			stateOfSaveBtn();
		})
	});
})