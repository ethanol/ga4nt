let ports = [];
function initialListener(port) {
	ports.push(port);
}
browser.runtime.onConnect.addListener(initialListener);

localStorage.removeItem('pinned');

// TODO: Make random less random
function randomFilename(titles) {
	titles = titles.filter( title => {
		let titleStored = JSON.parse(localStorage.getItem(title.title));
		if (titleStored != null && 'use' in titleStored && titleStored.use == true) {
			return true;
		} else if (titleStored != null && 'use' in titleStored && titleStored.use == false) {
			return false;
		} else {
			return true;
		}
	});
	let randomTitle = titles[ Math.floor( Math.random() * titles.length ) ];
	let randomNumber = (Math.floor( Math.random() * randomTitle.amount ) + 1);
	randomNumberString = randomNumber.toString();
	while (randomNumberString.length < 3) randomNumberString = "0" + randomNumberString;
	return {
		title: randomTitle.title,
		number: randomNumber,
		filename: randomTitle.title.replace(/-/g, '') + randomNumberString + '.jpg',
	}
}

function fetchImage(fileMeta) {
	return new Promise( (resolve, reject) => {
		fetch(`https://dpadar.gitlab.io/ga4nt/${fileMeta.filename}`
		).then( response => response.blob()
		).then( blob => {
			fileMeta.url = URL.createObjectURL(blob);
			fileMeta.timestamp = Date.now();
			fileMeta.pinned = false;
			resolve(fileMeta);
		});
	});
}

let imagesStack = [];
let historyStack = [];
let lastPoped = -1;
let prevNewestTimestamp;
function shiftToStack(image) {
	imagesStack.unshift(image);
	if (imagesStack.length >= 4) {
		imgToDel = imagesStack.pop();
	}
}
function pushToStack(image) {
	imagesStack.push(image);
	if (imagesStack.length >= 4) {
		imgToDel = imagesStack.shift();
	}
}
function shiftFromStack() {
	let newestTimestamp = imagesStack[imagesStack.length - 1].timestamp;
	if (newestTimestamp == prevNewestTimestamp) {
		if (lastPoped >= imagesStack.length - 1) {
			lastPoped = -1;
		}
	} else {
		lastPoped = -1;
		prevNewestTimestamp = imagesStack[imagesStack.length - 1].timestamp;
	}
	let img = imagesStack[++lastPoped];
	let imgMeta = {
		title: img.title,
		number: img.number,
	}
	historyStack.push(imgMeta);
	if (historyStack.length == 2) {
		img.previous = historyStack.shift();
	}
	return img;
}

let revokationCandidates = [];
let revokerRefs = {};
function serve(port, titles) {
	let img;
	let pinned = localStorage.getItem('pinned');
	if (pinned) {
		img = JSON.parse(pinned);
		if (imagesStack.length < 2) {
			fetchImage(randomFilename(titles)).then(pushToStack);
		}
	} else {
		img = shiftFromStack();
		fetchImage(randomFilename(titles)).then(pushToStack);
	}
	titlesList = titles.map( (title) => {
		return title.title;
	});
	img.allTitles = titlesList;
	port.postMessage(img);

	function revoker(tabId, info) {
		if ( (tabId == port.sender.tab.id) && (!info || info.status != 'complete')) {
			let pinned = localStorage.getItem('pinned');
			if (pinned) {
				pinned = JSON.parse(pinned);
				if (pinned.timestamp == img.timestamp) {
					img.pinned = true;
				} else {
					img.pinned = false;
				}
			} else {
				img.pinned = false;
			}

			if (img.pinned != true) {

				// Adding current image to revokationCandidates,
				// if it's not yet there
				let isRC;
				for (rc in revokationCandidates) {
					if (rc.timestamp == img.timestamp) {
						isRC = true;
					}
				}
				if (!isRC) {
					revokationCandidates.push(img);
				}
				
				// Revoking URLs of each of revokationCandidates,
				// if they aren't in imagesStack
				revokationCandidates = revokationCandidates.filter( rc => {
					for (stackItem in imagesStack) {
						if (rc.timestamp == stackItem.timestamp) {
							return true;
						}
					}
					URL.revokeObjectURL(rc.url);
					return false;
				});

				browser.tabs.onRemoved.removeListener(revokerRefs[img.timestamp]);
				browser.tabs.onUpdated.removeListener(revokerRefs[img.timestamp]);
				delete revokerRefs[img.timestamp];
			}
		}
	}

	revokerRefs[img.timestamp] = revoker;
	browser.tabs.onRemoved.addListener(revokerRefs[img.timestamp]);
	browser.tabs.onUpdated.addListener(revokerRefs[img.timestamp]);
}

fetch('https://dpadar.gitlab.io/ga4nt/listing.json').then(response => response.json()
).then( titles => {
	Promise.race([
		fetchImage(randomFilename(titles)).then(pushToStack),
		fetchImage(randomFilename(titles)).then(pushToStack),
		fetchImage(randomFilename(titles)).then(pushToStack),
	]).then( () => {
		browser.runtime.onConnect.addListener( port => {
			serve(port, titles);
			port.onMessage.addListener( () => {
				Promise.race([
					fetchImage(randomFilename(titles)).then(shiftToStack),
					fetchImage(randomFilename(titles)).then(shiftToStack),
					fetchImage(randomFilename(titles)).then(shiftToStack),
				]).then( () => {
					port.postMessage('reload');
				});
			});
		});
		browser.runtime.onConnect.removeListener(initialListener);
		ports.forEach( port => {
			serve(port, titles);
		});
	})
});